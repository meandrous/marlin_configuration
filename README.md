# BRF Marlin Configuration

Custom configuration files for BRF ANET A8. These are derived from the marlin_configuration GitHub project. The files in this repository go in the https://github.com/MarlinFirmware/Marlin.git Marlin sub-directory.